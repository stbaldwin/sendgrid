module Sendgrid
  class Address
    include JSON::Serializable
    property email : String
    property name : String?

    def initialize(@email, @name)
    end

    def initialize(@email)
    end
  end

  class Personalization
    include JSON::Serializable
    property to : Array(Address)
    property cc : Array(Address)?
    property bcc : Array(Address)?
    property subject : String?
    property headers : Hash(String, String)?
    property substitutions : Hash(String, String)?
    property send_at : Int64?

    def initialize(to : Address)
      @to = [to]
    end
  end

  class Content
    include JSON::Serializable
    property type : String
    property value : String

    def initialize(@value)
      @type = "text/plain"
    end
  end

  class Attachment
    include JSON::Serializable
    property content : String
    property type : String?
    property filename : String
    property disposition : String?
    property content_id : String?
  end

  class MailSettings
    class Bcc
      include JSON::Serializable
      property enable : Bool
      property email : String
    end

    class BypassListManagement
      include JSON::Serializable
      property enable : Bool
    end

    class SandboxMode
      include JSON::Serializable
      property enable : Bool
    end

    class Footer
      include JSON::Serializable
      property enable : Bool
      property text : String?
      property html : String?
    end

    class SpamCheck
      include JSON::Serializable
      property enable : Bool
      property threshold : Int32
      property post_to_url : String
    end

    include JSON::Serializable
    property bcc : Bcc?
    property bypass_list_management : BypassListManagement?
    property footer : Footer?
    property sandbox_mode : SandboxMode?
    property spam_check : SpamCheck?
  end

  class TrackingSettings
    class ClickTracking
      include JSON::Serializable
      property enable : Bool
      property enable_text : Bool
    end

    class OpenTracking
      include JSON::Serializable
      property enable : Bool
      property substitution_tag : String
    end

    class SubscriptionTracking
      include JSON::Serializable
      property enable : Bool
      property text : String?
      property html : String?
      property substitution_tag : String?
    end

    class Ganalytics
      include JSON::Serializable
      property enable : Bool
      property utm_source : String?
      property utm_medium : String?
      property utm_term : String?
      property utm_content : String?
      property utm_campaign : String?
    end

    include JSON::Serializable
    property click_tracking : ClickTracking?
    property open_tracking : OpenTracking?
    property subscription_tracking : SubscriptionTracking?
    property ganalytics : Ganalytics?
  end

  class Message
    include JSON::Serializable
    property personalizations : Array(Personalization)
    property from : Address
    property reply_to : Address?
    property subject : String
    property content : Array(Content)
    property attachments : Array(Attachment)?
    property template_id : String?
    property sections : Hash(String, String)?
    property headers : Hash(String, String)?
    property categories : Array(String)?
    property send_at : Int64?
    property batch_id : String?
    property mail_settings : MailSettings?
    property tracking_settings : TrackingSettings?

    def initialize(@from, to : Address, @subject, content : String)
      @personalizations = [Personalization.new(to)]
      @content = [Content.new(content)]
      @send_at = Time.now.epoch
    end

    def initialize(from, to : String, subject, content : String)
      initialize(from, Address.new(to), subject, content)
    end
  end
end
