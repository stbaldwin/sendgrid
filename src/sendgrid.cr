require "json"
require "http/client"
require "./sendgrid/*"

module Sendgrid
  VERSION = "0.1.0"
  URI = "https://api.sendgrid.com/v3/mail/send"

  class Client
    class Error
      include JSON::Serializable
      property field : String?
      property message : String?
    end

    class ErrorResponse
      include JSON::Serializable
      errors : Array(Error)
    end

    property api_key : String
    property from : Address
    property headers : HTTP::Headers

    def self.build_from_env
      if api_key = ENV["SENDGRID_API_KEY"]?
        if from_address = ENV["SENDGRID_FROM_ADDRESS"]?
          from_name = ENV["SENDGRID_FROM_NAME"]?
          return self.new(api_key, Address.new(from_address, from_name))
        end
      end
      raise "Missing environment variable(s)."
    end

    def initialize(@api_key, @from)
      @headers = HTTP::Headers{"Authorization" => "Bearer #{api_key}", "Content-Type" => "application/json"}
    end

    def send(message : Message)
      ret = HTTP::Client.post(URI, @headers, message.to_json.to_s)
      pp! ret
    end

    def send(to : Address, subject : String, content : String)
      send(Message.new(from, to, subject, content))
    end

    def send(to : String, subject : String, content : String)
      send(Address.new(to), subject, content)
    end
  end
end
